# borgmatic
[![Matrix](https://img.shields.io/matrix/discussion:etke.cc?logo=matrix&server_fqdn=matrix.org&style=for-the-badge)](https://matrix.to/#/#discussion:etke.cc) [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/etkecc)

borgmatic is simple, configuration-driven backup software for servers and workstations. Protect your files with client-side encryption. Backup your databases too. Monitor it all with integrated third-party services.

[Website](https://torsion.org/borgmatic/)

[Source code](https://projects.torsion.org/borgmatic-collective/borgmatic)

# docker image tags

Docker image tags represent postgres client version included in the image. The `latest` tag has the latest stable version

# schedule

This repository has scheduled build, so docker image updated weekly
